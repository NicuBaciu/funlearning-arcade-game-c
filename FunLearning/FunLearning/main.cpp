#include <SFML\Graphics.hpp>
#include <iostream>
#include <cmath>

#define width 860
#define height 590

#define xcenter 436
#define ycenter 590
#define radius 120
#define degtorad 0.0174

#define nrstars 10

using namespace sf;

class stars
{
	double v;

public:
	double xs, ys;

	int value;

	CircleShape c;

	stars();
	stars(int x, int y, int val);

	void draw_star(RenderWindow &app);

	void next_position();

	bool isvisible(int w, int h);
};

void draw(RenderWindow &app);
void draw_startmenu(RenderWindow &app);
void draw_game(RenderWindow &app);
void draw_rules(RenderWindow &app);
void draw_explosion(RenderWindow &app, int x, int y, int i, int j);
void add_button(int x, int y, int xtext, int ytext, String s, RenderWindow &app, int sx, int sy, double textscale = 1, int alp = 255);
void add_text(String s, int xtext, int ytext, RenderWindow &app, Color textcolor = Color::Black, double textscale = 1);

void init_graphics();
void init_colors();

int collision();

double fade_in = 0;
double alpha = 164;

bool startmenu = true;
bool game = false;
bool rules = false;

bool isshooting = false;
bool isexplo = false;

bool winner = false;

int direction = -1;

double xarrow;
double yarrow;

int iexplo = 0;
int jexplo = 0;

int xexplo = -1;
int yexplo = -1;

int score = 0;

double n, m;

stars *s[11];

stars *rule[13];

String valuetypes[13] = { " deci", "deca", "centi", "hecto", " mili", " kilo", "micro", "mega", "nano", " giga", "pico", " tera"};

Color valuecolor[13];

bool nobg = false;

Texture bg;
Sprite background;
Texture tsun;
Sprite sun;
Texture tarrow;
Sprite arrow;
Texture texplo;
Sprite explo;

Texture tstar;

Texture twin;
Sprite win;

Font f1;

int main()
{
	srand(time(0));

	RenderWindow app(VideoMode(860, 590), "FunLearning");
	app.setFramerateLimit(60);

	init_colors();

	init_graphics();

	for (int i = 1; i <= nrstars; i++)
	{
		s[i] = new stars();
	}

	while (app.isOpen())
	{
		Vector2i pos = Mouse::getPosition(app);
		int mousex = pos.x;
		int mousey = pos.y;

		Event e;

		while (app.pollEvent(e))
		{
			if (e.type == Event::Closed)
			{
				app.close();
			}
			if (e.type == Event::MouseButtonPressed)
			{
				if (e.key.code == Mouse::Left)
				{
					if (mousex >= 365 && mousex <= 515 && mousey >= 405 && mousey <= 475 && game == false && rules == false)
					{
						std::cout << "Start!" << '\n';

						if (nobg)
						{
							startmenu = false;

							game = true;
						}
					}
					if (mousex >= 355 && mousex <= 525 && mousey >= 485 && mousey <= 530 && game == false && rules == false)
					{
						std::cout << "How to!" << '\n';

						if (nobg)
						{
							startmenu = false;

							rules = true;
						}
					}
					if (mousex >= 725 && mousex <= 795 && mousey >= 535 && mousey <= 565 && game == false && startmenu == false)
					{
						std::cout << "Back Rules" << '\n';

						startmenu = true;

						rules = false;
					}
				}
			}
			if (e.type == Event::KeyPressed)
			{
				if (!winner)
				{
					if (e.key.code == Keyboard::Space)
					{
						std::cout << "Space" << '\n';

						m = (ycenter - yarrow) / (xcenter - xarrow);

						//m = tan(alpha*degtorad);

						n = ycenter - m * xcenter;

						isshooting = true;
					}
				}
				else
				{
					startmenu = true;
					game = false;

					winner = false;

					score = 0;
				}
			}
		}

		draw(app);

		app.display();
	}

	for (int i = 1; i <= nrstars; i++)
		delete s[i];

	return 0;
}

// ------------------- INIT GRAPHICS --------------- //

void init_graphics()
{

	// Start Menu //
	bg.loadFromFile("Images/background.jpg");
	background.setTexture(bg);

	background.setPosition(0, 0);


	// Game Menu // 

	// Background //
	bg.loadFromFile("Images/background.jpg");
	background.setTexture(bg);

	background.setPosition(0, 0);

	// Planet //
	tsun.loadFromFile("Images/sun.png");
	sun.setTexture(tsun);
	sun.setTextureRect(IntRect(350, 680, 320, 324));

	sun.setPosition(Vector2f(300, 600));
	sun.rotate(310);
	sun.setScale(0.6, 0.6);

	// Stars //
	tstar.loadFromFile("Images/jpg/orange_bw.png");


	// Arrow //
	tarrow.loadFromFile("Images/arrow.png");
	arrow.setTexture(tarrow);
	arrow.setOrigin(Vector2f(19, 19));

	// Explosion //

	texplo.loadFromFile("Images/exp.png");
	explo.setTexture(texplo);
	explo.setOrigin(64, 64);

	// Win //

	twin.loadFromFile("Images/win.png");
	win.setTexture(twin);
	win.setPosition(130, 50);

	//Font
	f1.loadFromFile("fonts/VIDEOPHREAK.ttf");
}

// -------------- START MENU ----------------- //

void draw_startmenu(RenderWindow &app)
{
	if (!nobg)
	{
		if (fade_in >= 255)
		{
			fade_in = 255;
			nobg = true;
		}
		background.setColor(Color(fade_in, fade_in, fade_in));
		if (fade_in < 255) fade_in += 3;
	}
	
	Color textcolor = Color(149, 175, 245, fade_in);

	app.draw(background);

	add_text("Fun Learning", 150, 140, app, textcolor, 2.6);
	add_button(365, 405, 383, 418, "START!", app, 150, 70, 1, fade_in);

	add_button(355, 485, 370, 495, "How to play", app, 170, 50, 0.7, fade_in);
}


// ---------------- GAME MENU ----------------- //

void draw_game(RenderWindow &app)
{
	app.draw(background);
	app.draw(sun);

	add_text("Score: " + std::to_string(score), 10, 550, app, Color::White);
	
	if (!isshooting)
	{
		xarrow = xcenter + radius*cos(alpha*degtorad);
		yarrow = ycenter - radius*sin(alpha*degtorad);

		arrow.setPosition(Vector2f(xarrow, yarrow));
		arrow.setRotation(-alpha);

		alpha += direction * 2;

		if (alpha <= 15)
		{
			direction = -direction;
			alpha = 16;
		}

		if (alpha >= 165)
		{
			direction = -direction;
			alpha = 164;
		}
	}
	else
	{
		if (xarrow >= -10 && yarrow >= -10 && xarrow <= 860)
		{
			if (m != 0)
			{
				if (alpha == 90)
				{
					yarrow -= 7;
				}
				else
				{
					if (alpha > 90)
						xarrow += cos(degtorad*alpha)*10;
					else
						xarrow += cos(degtorad*alpha)*10;

					yarrow = m*xarrow + n;
				}
			}
			else
				yarrow-=0.15;

			arrow.setPosition(Vector2f(xarrow, yarrow));

			int col = collision();

			if (col != -1 && yarrow >= -10 && xarrow >= -10 && xarrow <= 860)
			{
				xexplo = s[col]->xs;
				yexplo = s[col]->ys;

				if (s[col]->value <= 6)
				{
					if (s[col]->value % 2 == 0)
					{
						score -= s[col]->value / 2 + 1;
					}
					else
					{
						score += s[col]->value / 2 + 1;
					}
				}
				else
				{
					if (s[col]->value % 2 == 0)
					{
						score -= 3 * (s[col]->value / 2 - 1);
					}
					else
					{
						score += 3 * (s[col]->value / 2 - 1);
					}
				}

				delete s[col];

				s[col] = new stars();

				isshooting = false;

				isexplo = true;
			}
			else
				isexplo = false;
		}
		else
		{
			isshooting = false;
		}
	}
		
	app.draw(arrow);

	for (int i = 1; i <= nrstars; i++)
	{
		if (s[i]->isvisible(width, height))
		{
			s[i]->draw_star(app);
		}
		else if (s[i]->xs >= width + 30)
		{
			delete s[i];
			s[i] = new stars();
		}

		if (s[i]->xs <= width + 30)
			s[i]->next_position();
	}

	if (isexplo)
	{
		if (iexplo <= 3 && jexplo <= 3)
		{
			draw_explosion(app, xexplo, yexplo, iexplo, jexplo);

			if (jexplo == 3)
			{
				iexplo+=1;
				jexplo = 0;
			}
			else
			{
				jexplo+=1;
			}
		}
		else
		{
			iexplo = 0;
			jexplo = 0;
			isexplo = false;
		}
	}

	if (score == 100)
	{
		app.draw(win);
		winner = true;
	}
}

// ----------- DRAW MENUS ----------- //

void draw(RenderWindow &app)
{
	if (startmenu)
	{
		draw_startmenu(app);
	}
	if (game)
	{
		draw_game(app);
	}
	if (rules)
	{
		draw_rules(app);
	}
}

// -------------- DRAW TEXT FUNCTION ------------- //

void add_text(String s, int xtext, int ytext, RenderWindow &app, Color textcolor, double textscale)
{
	Text st1;

	st1.setString(s);
	st1.setFont(f1);
	st1.setScale(textscale, textscale);
	st1.setPosition(xtext, ytext);
	st1.setFillColor(textcolor);

	app.draw(st1);
}

// ----------------- DRAW BUTTON FUNCTION ---------- //

void add_button(int x, int y, int xtext, int ytext, String s, RenderWindow &app, int sx, int sy, double textscale, int alp)
{
	Text st1;

	st1.setString(s);
	st1.setFont(f1);
	st1.setScale(textscale, textscale);
	st1.setPosition(xtext, ytext);
	st1.setFillColor(Color(0, 0, 0, alp));

	RectangleShape test_btn;

	Color greenish(100, 200, 70, alp);

	test_btn.setPosition(x, y);
	test_btn.setSize(Vector2f(sx, sy));
	test_btn.setOutlineColor(Color(0, 0, 0, alp));
	test_btn.setFillColor(greenish);
	test_btn.setOutlineThickness(1);

	app.draw(test_btn);
	app.draw(st1);
}

// -------------- DRAW STARS FUNCTION -------- //

void stars::draw_star(RenderWindow &app)
{
	c.setPosition(Vector2f(this->xs, this->ys));

	app.draw(c);

	add_text(valuetypes[this->value], this->xs - 16, this->ys - 14, app, Color::White, 0.45);
}

// ------ IS INVISIBLE ------ //

bool stars::isvisible(int w, int h)
{
	if (this->xs >= -30 && this->xs <= w + 30  && this->ys >= 30 && this->ys <= 400)
	{
		return true;
	}
	return false;
}

// -------- NEXT POS ------- //

void stars::next_position()
{
	this->xs += v;
}

// -------------- STARS CONSTRUCTOR ------- //

stars::stars()
{
	this->xs = (1000 * rand() * rand()) % 565 - 600;

	this->ys = (1000 * rand() * rand()) % 350 + 50;

	this->v = (rand()%15 + 5) / 7;

	this->value = rand() % 12;

	c.setRadius(28);

	c.setOrigin(28, 28);
	c.setPointCount(8);
	c.setTexture(&tstar);

	c.setFillColor(valuecolor[this->value]);
}

stars::stars(int x, int y, int val)
{
	this->xs = x;
	this->ys = y;

	this->v = 0;

	this->value = val;

	c.setRadius(28);

	c.setOrigin(28, 28);
	c.setPointCount(8);
	c.setTexture(&tstar);

	c.setFillColor(valuecolor[this->value]);
}

// --------- COLOR INIT --------- //

void init_colors()
{
	valuecolor[0] = Color(230, 25, 75);
	valuecolor[1] = Color(60, 180, 75);
	valuecolor[2] = Color(225, 225, 25);
	valuecolor[3] = Color(0, 130, 200);
	valuecolor[4] = Color(245, 130, 48);
	valuecolor[5] = Color(145, 30, 180);
	valuecolor[6] = Color(70, 240, 240);
	valuecolor[7] = Color(240, 50, 230);
	valuecolor[8] = Color(210, 245, 60);
	valuecolor[9] = Color(250, 190, 255);
	valuecolor[10] = Color(0, 128, 128);
	valuecolor[11] = Color(230, 190, 255);
}

// ----------- COLLISION ------------ //

int collision()
{
	for (int i = 1; i <= 10; i++)
	{
		if ((xarrow - s[i]->xs)*(xarrow - s[i]->xs) + (yarrow - s[i]->ys)*(yarrow - s[i]->ys) < 2209)
		{
			return i;
		}
	}

	return -1;
}

// ------------ DRAW EXPLOSION -------------- //

void draw_explosion(RenderWindow &app, int x, int y, int i, int j)
{
	explo.setPosition(x, y);

	explo.setTextureRect(IntRect(j * 128, i * 128, 128, 128));
	app.draw(explo);
}

// ------------- DRAW RULES ----------------- //

void draw_rules(RenderWindow &app)
{
	app.draw(background);

	add_button(725, 535, 733, 535, "Back", app, 70, 30, 0.7);

	add_text("Scopul jocului:", 10, 10, app, Color::White);

	add_text("Jocul are ca scop obtinerea scorului 100.", 10, 60, app, Color::White, 0.7);
	add_text("Punctele se obtin prin lovirea cu sageata", 10, 90, app, Color::White, 0.7);
	add_text("verde a corpurilor colorate de pe ecran. ", 10, 120, app, Color::White, 0.7);
	add_text("Fiecare corp are marcat pe el cate un multiplu", 10, 150, app, Color::White, 0.7);
	add_text("sau o subdiviziune a unei unitati. Fiecarui", 10, 180, app, Color::White, 0.7);
	add_text("corp ii este asociat un scor egal cu puterea", 10, 210, app, Color::White, 0.7);
	add_text("lui 10 aferenta subdiviziunii / multiplului", 10, 240, app, Color::White, 0.7);
	add_text("unitatii. Astfel, unele corpuri au punctaj", 10, 270, app, Color::White, 0.7);
	add_text("negativ. Jocul este castigat doar la obtinerea", 10, 300, app, Color::White, 0.7);
	add_text("a exact 100 de puncte. Lansarea sagetii se", 10, 330, app, Color::White, 0.7);
	add_text("face la apasarea tastei SPACE.", 10, 360, app, Color::White, 0.7);


	for (int i = 1; i <= 12; i++)
	{
		int xscore;

		rule[i] = new stars(30 + 70*(i-1), 450, i-1);
		rule[i]->draw_star(app);

		if (rule[i]->value < 6)
		{
			if (rule[i]->value % 2 == 0)
			{
				xscore = rule[i]->value / 2 + 1;
				xscore = -xscore;
			}
			else
			{
				xscore = rule[i]->value / 2 + 1;
			}
		}
		else
		{
			if (rule[i]->value % 2 == 0)
			{
				xscore = -3 * (rule[i]->value / 2 - 1);
			}
			else
			{
				xscore = 3 * (rule[i]->value / 2 - 1);
			}
		}

		add_text(std::to_string(xscore), 20 + 70 * (i - 1), 480, app, Color::White, 0.7);
	}
}